
const int PIN_IN = 4;

int state = 0;
bool inputSignal = false;
int recData[100];
long recTime[100]; 

void setup()
{
  // put your setup code here, to run once:
  Serial.begin(9600);
  delay(2000);
  Serial.println("Started");
  pinMode(PIN_IN, INPUT);
  Serial.println("Setting pin modes complete...");
}

void loop()
{
  // put your main code here, to run repeatedly:
  bool now = digitalRead(PIN_IN);
  if(now == true)
  {
    for(int i = 0; i<100; i++)
    {
      recData[i] = digitalRead(PIN_IN);
      recTime[i] = micros();
      delayMicroseconds(60);
    }
    for(int i = 0; i<100; i++)
    {
      String currentTime = String(recTime[i]);
      String currentData = String(recData[i]);
      Serial.println(currentTime + "," + currentData);
    }
  }
}
