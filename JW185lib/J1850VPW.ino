

const int PIN_OUT = 5;
const int PIN_IN = 4;

String msg = "";
boolean isValid = false;

void setup()
{
    Serial.begin(9600);
    delay(2000);
    Serial.println("Started");

    pinMode(PIN_OUT, OUTPUT);
    pinMode(PIN_IN, INPUT);
    Serial.println("Set pin modes to output");
}

byte CRC8(const byte *data, byte len)
{
    byte crc = 0x00;

    while (len--)
    {
        byte extract = *data++;

        for (byte tempI = 8; tempI; tempI--)
        {
            byte sum = (crc ^ extract) & 0x01;
            crc >>= 1;

            if (sum)
            {
                crc ^= 0x8C;
            }

            extract >>= 1;
        }
    }

    return crc;
}

void parseResponse(String before, int after[])
{
    Serial.println("Message byteCount: " + String(before.length()) + "/8 = " + String(before.length() / 8));

    for (int byteCount = 0; byteCount < before.length() / 8; byteCount++)
    {
        int currentByte = 0;

        for (int bitCount = 0; bitCount < 8; bitCount++)
        {
            bitWrite(currentByte, 8 - (bitCount + 1), String(before.charAt((byteCount * 8) + bitCount)).toInt());
        }

        after[byteCount] = currentByte;
    }
}

void loop()
{
    while (Serial.available() > 0)
    {
        msg += (char)Serial.read();
    }

    if (msg == "itemp")
    {
        Serial.println(msg + "== 'itemp'");
        sendPacket(0x01, 0x0F);
    }
    else if (msg == "speed")
    {
        sendPacket(0x01, 0x0D);
    }

    msg = "";
}

// Packet:
//     |----------------Header-----------------|
// SOD Priority/Type TargetAddress SourceAddress Mode PID CRC EOD
int sendPacket(int mode, int pid)
{                        // #76543210
    int priority = 0x68; //0b01101000
    //int priority = 0b00001100;
    int targetAddr = 0x6A; // 0x10 = PCM
                           // 0xF1 = Off-Board Tool
    int sourceAddr = 0xF1; // [EDIT SCRATCH THIS]: Anything between 0xF0 to 0xFF works just
    // just to say its a scan tool
    int packet[8 * 6];   // 8 bits per byte X 6 bytes
    byte packetBytes[6]; // The 6 bytes for CRC building
    byte crc = 0x00;

    // Build CRC ahead of time
    packetBytes[0] = priority;
    packetBytes[1] = targetAddr;
    packetBytes[2] = sourceAddr;
    packetBytes[3] = 0x01;
    packetBytes[4] = pid;

    crc = CRC8(packetBytes, 5);
    packetBytes[5] = crc;

    // Assemble packet from known request parameters
    // NOTE: 8 is the most significant (leading) bit
    for (int i = 8; i > 0; i--)
    {
        packet[0 + (8 - i)] = getBit(priority, i);
        packet[8 + (8 - i)] = getBit(targetAddr, i);
        packet[16 + (8 - i)] = getBit(sourceAddr, i);
        packet[24 + (8 - i)] = getBit(mode, i);
        packet[32 + (8 - i)] = getBit(pid, i);
        packet[40 + (8 - i)] = getBit(crc, i);
    }

    Serial.println("Packet:");
    for (int i = 0; i < 6; i++)
    {
        Serial.print(String(packetBytes[i], HEX) + " ");
        String str_currentByte = "";
        byte by_currentByte = 0x00;

        for (int i_bit = 0; i_bit < 8; i_bit++)
        {
            str_currentByte = str_currentByte + String(packet[i + i_bit]);
        }
    }
    Serial.println();

    sendSOD();

    int expectedVoltage = -1;
    for (int i = 0; i < (sizeof(packet) / sizeof(int)); i++)
    {
        sendBit(packet[i], expectedVoltage);
        expectedVoltage = -expectedVoltage;
    }
    sendEOD();
    sendEOF();
    Serial.println("Sending packet complete");
}

void sendSOD()
{
    digitalWrite(PIN_OUT, HIGH);
    delayMicroseconds(200);
    digitalWrite(PIN_OUT, LOW);
}

void sendEOF()
{
    digitalWrite(PIN_OUT, HIGH);
    delayMicroseconds(280);
    digitalWrite(PIN_OUT, LOW);
}

void sendEOD()
{
    digitalWrite(PIN_OUT, LOW);
    delayMicroseconds(200);
    digitalWrite(PIN_OUT, LOW);
}

void sendBit(int value, int expectedVoltage)
{
    if (expectedVoltage == 1)
    {
        digitalWrite(PIN_OUT, HIGH);

        if (value == 1)
        {
            delayMicroseconds(63);
        }
        else
        {
            delayMicroseconds(125);
        }
    }
    else
    {
        digitalWrite(PIN_OUT, LOW);

        if (value == 1)
        {
            delayMicroseconds(125);
        }
        else
        {
            delayMicroseconds(63);
        }
    }
}

// NOTE: 8 is the most significant (leading) bit
int getBit(int number, int n)
{
    int mask = 1;
    mask = mask << (n - 1);

    return (mask & number) >> (n - 1);
}
