﻿using System;
using System.IO;
using CsvHelper;

namespace DifferenceFinder {
    public class Program {
        public static void Main (string[] args) {
            var ArchiveListFile = Path.Combine (@"C:\Users\hari2\Desktop", "data.csv");
            if (File.Exists (ArchiveListFile)) {
                Console.WriteLine ("File Exists...");
                try {
                    TextReader reader = new StreamReader (ArchiveListFile);
                    var csvReader = new CsvReader (reader);
                    var records = csvReader.GetRecords<InputChangeData> ();
                    Console.Write ("Microseconds \t\t\t\t");
                    Console.Write ("Differnece \t\t\t\t");
                    Console.Write ("Data\n");
                    long previoustime = 0;
                    foreach (var record in records) {
                        Console.Write (record.time);
                        Console.Write ("\t\t\t\t");
                        var time = Convert.ToInt64(record.time);
                        Console.Write (time - previoustime);
                        previoustime = time;
                        Console.Write ("\t\t\t\t");
                        Console.Write (record.bit);
                        Console.Write ("\n");
                    }
                } catch (Exception ex) {
                    Console.WriteLine ("Exception");
                    Console.WriteLine (ex.Message);
                }
            }
        }
    }
}